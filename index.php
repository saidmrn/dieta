<html>
<head>
    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="css/materialize.min.css"  media="screen,projection"/>

    <link type="text/css" rel="stylesheet" href="css/estilo.css"  media="screen,projection"/>

    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta charset="UTF-8">

</head>

<body>
<nav>
    <div class="nav-wrapper">
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>

        <ul id="nav-mobile"  class=" hide-on-med-and-down" style="margin-left: 30%">
            <li><a id="AdicionarAlimentoNavBar">Adicionar Alimento</a></li>
            <li><a id="CriarRefeicaoNavBar">Criar Refeições</a></li>
        </ul>

        <ul class="side-nav" id="mobile-demo">
            <li><a id="AdicionarAlimentoNavBarcel">Adicionar Alimento</a></li>
            <li><a id="CriarRefeicaoNavBarcel">Criar Refeições</a></li>

        </ul>
    </div>
</nav>
<!--Import jQuery before materialize.js-->

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="js/jquery.cookie.js"></script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript" src="js/materialize.min.js"></script>
<script type="text/javascript" src="js/script.js"></script>








<!TABELA DIETA !>
<div id="area_refeicoes" </div>




</div>
<div id="modalAddAlimento" class="modal modal-fixed-footer">
    <div class="modal-content">
        <h4>Adicionar Alimento</h4>

        <div style="text-align: center">



            <select name="SelecionarAlimentos">

                <?PHP
                include "conexao.php";

                $conn->query("SET names 'utf8'");
                $sql = "select F1,Alimento,Caloria,Proteina,Gordura,carb from alimentos";
                $resultado = $conn->query($sql);


                if($resultado->num_rows > 0)
                {
        echo "                <option value=\"\" disabled selected>Selecionar Alimento</option>";
                    while($linha = $resultado->fetch_assoc())
                    {

                        echo "<option value={$linha['F1']} data-calorias='{$linha['Caloria']}' data-proteinas='{$linha['Proteina']}' data-carboidratos='{$linha['carb']}' data-gordura='{$linha['Gordura']}'>{$linha['Alimento']}</option>";
                    }
                }
                $conn->close();
                    ?>
            </select>
            <select name="SelecionarRefeicao">
                <option value="" disabled selected>Selecionar Refeição</option>

            </select>

            <div class="input-field col s6">
                <input id="quantidade" type="text" class="validate">
                <label>Quantidade (g)</label>
            </div>

            <h6 class="center">Porção de 100 g</h6>
            <h6 id="CaloriasAddAlimento" class="center"></h6>
            <h6 id="ProteinasAddAlimento" class="center"></h6>
            <h6 id="GordurasAddAlimento" class="center"></h6>
            <h6 id="CarboidratosAddAlimento" class="center"></h6>


            <div id="chart_macros_AddAlimento"></div>

            <a  id="BtnAddAlimentoModal" class="waves-effect waves-light btn-large center" style="margin: 0 auto;text-align: center">Adicionar</a>

        </div>
    </div>

</div>


<div id="modalCriarRefeicao" class="modal modal-fixed-footer" style="width: 800px;height: 1200px;">
    <div class="modal-content">
        <h4>Quantas Refeições Deseja Criar?</h4>

        <div style="text-align: center">



            <select name="SelectnumeroRefeicoes" class="center">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                <option value="7">7</option>
                <option value="8">8</option>
                <option value="9">9</option>
                <option value="10">10</option>
            </select>


            <div id="DivTextBoxAddRefeicao"></div>
            <a id="BtnAddRefeicoesModal" class="waves-effect waves-light btn-large center" style="margin: 0 auto;text-align: center">Adicionar</a>

        </div>
    </div>

</div>


<div class="row">
<div class="col l8 m12 s12">
    <div id="chart_macros"></div>

</div>

</div>



</body>
</html>