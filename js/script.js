
$(document).ready(function(){


    $('select').material_select();
    $('.button-collapse').sideNav();








// abrir modal da navbar para add alimentos

$('#AdicionarAlimentoNavBar').click(function () {


    $('#modalAddAlimento').openModal();
    $('#btnaddalimentomodal').hide();
    PopularRefeicoesAddAlimento();
});

    $('#AdicionarAlimentoNavBarcel').click(function () {


        $('#modalAddAlimento').openModal();
        $('#btnaddalimentomodal').hide();
        PopularRefeicoesAddAlimento();
    });





// INICIO MODAL ALIMENTOS
    // atualizar info no modal alimentos
    $( "select[name=SelecionarAlimentos]" ).change(function() {


        //alert($("select[name=SelecionarAlimentos] option:selected" ).val());

        var caloriasAddAlimento = $("select[name=SelecionarAlimentos] option:selected" ).data('calorias');
         var proteinasAddAlimento = $("select[name=SelecionarAlimentos] option:selected" ).data('proteinas');
        var  gordurasAddAlimento= $("select[name=SelecionarAlimentos] option:selected" ).data('gordura');
        var carboidratosAddAlimento = $("select[name=SelecionarAlimentos] option:selected" ).data('carboidratos');

        $('#CaloriasAddAlimento').html('Calorias: '+caloriasAddAlimento + ' kcal');
        $('#ProteinasAddAlimento').html('Proteínas: '+proteinasAddAlimento + ' g');
        $('#CarboidratosAddAlimento').html('Carboidratos: '+carboidratosAddAlimento + ' g');
        $('#GordurasAddAlimento').html('Gorduras: '+gordurasAddAlimento + ' g');




        google.charts.load('current', {'packages':['corechart']});

        // Set a callback to run when the Google Visualization API is loaded.
        google.charts.setOnLoadCallback(drawChart);

        // Callback that creates and populates a data table,
        // instantiates the pie chart, passes in the data and
        // draws it.
        function drawChart() {

            // Create the data table.
            var data = new google.visualization.DataTable();
            data.addColumn('string', 'Topping');
            data.addColumn('number', 'Slices');
            data.addRows([
                ['Proteína', proteinasAddAlimento],
                ['Gordura', gordurasAddAlimento],
                ['Carboidratos', carboidratosAddAlimento]
            ]);

            // Set chart options
            var options = {
                'title': 'Divisão de Macros',
                'width': 450,
                'height': 300
            };

            // Instantiate and draw our chart, passing in some options.
            var chart = new google.visualization.PieChart(document.getElementById('chart_macros_AddAlimento'));
            chart.draw(data, options);
        }
    });


    function PopularRefeicoesAddAlimento() {
        var refeicoes = JSON.parse($.cookie('refeicoes'));

        $('select[name=SelecionarRefeicao]').empty();
        $('select[name=SelecionarRefeicao]').append('<option value="" disabled selected>Selecionar Refeição</option>');
        $(refeicoes).each(function () {


            $('select[name=SelecionarRefeicao]').append($('<option>', {
                value: this.id,
                text: this.nome
            }));
            $('select').material_select();
        });
    }



    $('#BtnAddAlimentoModal').click(function () {
        var AlimentoAddAlimento =   $("select[name=SelecionarAlimentos] option:selected" ).text();
        var quantidade = $('#quantidade').val();
        var idalimento = $("select[name=SelecionarAlimentos] option:selected" ).val();

        var caloriasAddAlimento = $("select[name=SelecionarAlimentos] option:selected" ).data('calorias');

        var refeicaoid = $("select[name=SelecionarRefeicao] option:selected" ).val();
        var refeicaotexto =  $("select[name=SelecionarRefeicao] option:selected" ).text();
        var Alimentos = {};



           if(refeicaoid == '') {
               Materialize.toast('Selecione uma Refeição para Adicionar o Alimento!', 4000);



           }else {


               if (caloriasAddAlimento == null) {
                   Materialize.toast('Selecione um Alimento!', 4000);


               } else {


                        if(quantidade == '')
                        {
                            quantidade = 100;
                            Materialize.toast("Adicionada Quantidade Padrão(100 g)",7000);
                        }



                       if (typeof $.cookie('alimentos') === 'undefined') {


                           var Alimento = [{
                               'id': 0,
                               'refeicao': refeicaoid,
                               'idalimento':idalimento,
                               'quantidade': quantidade
                           }];
                           $.cookie('alimentos', JSON.stringify(Alimento));

                       } else {

                           Alimentos = JSON.parse($.cookie('alimentos'));
                           var id = Alimentos.length;
                           Alimentos.push({
                               'id': id,
                               'refeicao': refeicaoid,
                               'idalimento':idalimento,
                               'quantidade': quantidade
                           });
                           $.cookie('alimentos', JSON.stringify(Alimentos));

                       }


Materialize.toast('Você Adicionou '+AlimentoAddAlimento + " em "+ refeicaotexto,7000);


               }// se selecionou alimento
           }// se selecionou refeicao


        AtualizarTabela();

    });

// FIM MODAL ALIMENTOS


    // MODAL ADICIONAR REFEICAO
    $('#CriarRefeicaoNavBar').click(function () {


        $('#modalCriarRefeicao').openModal();
        $('#BtnAddRefeicoesModal').hide();

    });
    $('#CriarRefeicaoNavBarcel').click(function () {


        $('#modalCriarRefeicao').openModal();
        $('#BtnAddRefeicoesModal').hide();

    });

    var numeroderefeicoesmodal;
 // gerar input's de acordo com o numero de refeições
    $( "select[name=SelectnumeroRefeicoes]" ).change(function() {

        numeroderefeicoesmodal= $(this).val();

        $('#DivTextBoxAddRefeicao').empty();
        for(var i=0;i<numeroderefeicoesmodal;i++)
        {

            var input = jQuery('<input  class="refeicao_jquery" placeholder="Refeição '+(i+1)+ '"</input>');
            jQuery('#DivTextBoxAddRefeicao').append(input);
        }
        $('#BtnAddRefeicoesModal').show();


    });


    $('#BtnAddRefeicoesModal').click(function () {

        $.removeCookie('alimentos', { path: '/' });
        $.removeCookie('refeicoes', { path: '/' });

        var ArrayObjectRefeicoes = [];
        $('.refeicao_jquery').each(function (index) {

            ArrayObjectRefeicoes.push({'id':index,'nome':$(this).val()});

        });

        $.cookie('refeicoes',JSON.stringify(ArrayObjectRefeicoes)); // salvar as refeições em um cookie
Materialize.toast("Você Adicionou "+$('.refeicao_jquery').length+" Refeições",4000);
AtualizarTabela();
    });



    // FIM MODAL ADICIONAR REFEICAO


    window.removeralimento = function (idalimento) {




        var alimentos = JSON.parse($.cookie('alimentos'));
        var tempalimentos = alimentos;



        for(var i=0;i<alimentos.length;i++)
        {


            if(alimentos[i].id == idalimento)
            {
                console.log("Alimento tabela:"+alimentos[i].id + "Alimento remover:"+idalimento);
                tempalimentos.splice(i,1);

            }
        }

        alimentos = tempalimentos;

        $.cookie('alimentos',JSON.stringify(alimentos));
        AtualizarTabela();
    };






    AtualizarTabela();
    function AtualizarTabela () {




        // limpar area da tabela
        $('#area_refeicoes').empty();
        // mensagem se a refeição estiver vazia

        $('#area_refeicoes').html('<h3 class="titulo_refeicao">Você não criou nenhuma refeição =(</h3>');



        var refeicoes = JSON.parse($.cookie('refeicoes'));


        if(refeicoes.length > 0)
        {
            $('#area_refeicoes').html('<h3 class="titulo_refeicao">Você Criou '+refeicoes.length+' Refeições,Agora basta adicionar os alimentos =)</h3>');

        }
        if (typeof $.cookie('alimentos') != 'undefined') {

            $('#area_refeicoes').html('');

        }

        var alimentos = JSON.parse($.cookie(('alimentos')));


        function getArrayIndexForKey(arr, key, val){
            for(var i = 0; i < arr.length; i++){
                if(arr[i][key] == val)
                    return i;
            }
            return -1;
        }

var tamanho = refeicoes.length;

        var temprefeicaoremover=[];
        for(var i=0;i<tamanho;i++)
        {

            var temalimento = false;

            for(var a=0;a<alimentos.length;a++)
            {


                if(alimentos[a].refeicao == refeicoes[i].id)
                {

                    temalimento = true;

                }
            } // fim for alimentos


            if(!temalimento)
            {

        temprefeicaoremover.push(refeicoes[i].id);

            } // fim if se tem alimento
        } // fim for refeicao


        tamanho = temprefeicaoremover.length;
        for(var i=0;i<tamanho;i++)
        {
            refeicoes.splice(getArrayIndexForKey(refeicoes,'id',temprefeicaoremover[i]),1);
        }


        // inicializar variaveis para produção do gráfico total de macros
        var carbtotal=0;
        var proteinatotal =0;
        var gorduratotal = 0;

        var row = jQuery('<div class="row"></div>');
      $(refeicoes).each(function (index,refeicao) {
        var titulo = jQuery('<div class="titulo_refeicao">'+refeicao.nome+'</div>');

          var areainterna = jQuery('<div class="col l6 s12 m12   "></div>');
          $(areainterna).append(titulo);

            var tabela;

          switch(index)
          {
              case 0:
                  tabela = jQuery('<table  class="bordered highlight" style="background-color: #D7BDE2;border-radius: 10px"</table></div></div>');
                  break;

              case 1:
                  tabela = jQuery('<table  class="bordered highlight" style="background-color: #AED6F1 ;border-radius: 10px"</table></div></div>');

                  break;
              case 2:
                  tabela = jQuery('<table class="bordered highlight" style="background-color: #A3E4D7;border-radius: 10px"/>');
                  break;
              case 3:

                  tabela = jQuery('<table class="bordered highlight" style="background-color: #A2D9CE;border-radius: 10px""/>');
                  break;
              case 4:
                  tabela = jQuery('<table class="bordered highlight" style="background-color: #A9DFBF;border-radius: 10px"/>');
                  break;
              case 5:
                  tabela = jQuery('<table class="bordered highlight" style="background-color: #ABEBC6.border-radius: 10px"/>');
                  break;
              case 6:
                  tabela = jQuery('<table class="bordered highlight" style="background-color:#F9E79F;border-radius: 10px"/>');
                  break;
              case 7:
                  tabela = jQuery('<table class="bordered highlight" style="background-color:#FAD7A0;border-radius: 10px"/>');
                  break;
              case 8:
                  tabela = jQuery('<table class="bordered highlight" style="background-color:#F5CBA7;border-radius: 10px"/>');
                  break;
              case 9:
                  tabela = jQuery('<table class="bordered highlight" style="background-color:#EDBB99;border-radius: 10px"/>');
                  break;
              case 10:
                  tabela = jQuery('<table class="bordered highlight" style="background-color:#E5E7E9;border-radius: 10px"/>');
                  break;


          }
          if ($(window).width() > 1000) {
              tabela.append(' <th >Alimento</th><th>Quantidade</th> <th >Calorias</th> <th >Proteínas</th> <th >Carboidratos</th> <th >Gorduras</th>');

          }

          $(alimentos).each(function (index,alimento) {
              if(refeicao.id == alimento.refeicao)
              {

                  var Alimento =   $("select[name=SelecionarAlimentos] option[value="+alimento.idalimento+']' ).text();
                  var quantidade = alimento.quantidade;
                  var caloriasAddAlimento =  $("select[name=SelecionarAlimentos] option[value="+alimento.idalimento+']' ).data('calorias');
                  var proteinasAddAlimento =  $("select[name=SelecionarAlimentos] option[value="+alimento.idalimento+']' ).data('proteinas');
                  var  gordurasAddAlimento=  $("select[name=SelecionarAlimentos] option[value="+alimento.idalimento+']' ).data('gordura');
                  var carboidratosAddAlimento =  $("select[name=SelecionarAlimentos] option[value="+alimento.idalimento+']' ).data('carboidratos');

                  if(quantidade == 0)
                  {
                      quantidade = 100;
                      CaloriasQuantidade = (quantidade * caloriasAddAlimento)/100;
                      ProteinasQuantidade = (quantidade * proteinasAddAlimento)/100;
                      GordurasQuantidade = (quantidade * gordurasAddAlimento)/100;
                      CarboidratosQuantidade = (quantidade * carboidratosAddAlimento)/100;
                  }else{
                      quantidade = alimento.quantidade;
                      CaloriasQuantidade = (quantidade * caloriasAddAlimento)/100;
                      ProteinasQuantidade = (quantidade * proteinasAddAlimento)/100;
                      GordurasQuantidade = (quantidade * gordurasAddAlimento)/100;
                      CarboidratosQuantidade = (quantidade * carboidratosAddAlimento)/100;

                  }


                  if($.isNumeric(CarboidratosQuantidade))
                  {
                      carbtotal +=CarboidratosQuantidade;

                  }
                  if($.isNumeric(ProteinasQuantidade))
                  {
                      proteinatotal += ProteinasQuantidade;

                  }
                  if($.isNumeric(GordurasQuantidade))
                  {
                      gorduratotal += GordurasQuantidade;

                  }




                  tabela.append('<tr   data-idalimento="'+alimento.id+'"><td>'+Alimento+'</td> <td>'+quantidade+' g</td><td>'+CaloriasQuantidade.toFixed(2)+' kcal</td><td>'+ProteinasQuantidade.toFixed(2)+' g</td><td>'+CarboidratosQuantidade.toFixed(2)+' g</td><td>'+GordurasQuantidade.toFixed(2)+' g</td><td><img onclick="removeralimento('+alimento.id +')" src="img/remove.png" width="30px"/></td></tr>');

              }


          });

          $(areainterna).append(tabela);
          $(row).append(areainterna);
            $('#area_refeicoes').append(row);
        });




        // gerar grafico macros
        DrawMacros(carbtotal,proteinatotal,gorduratotal);


    } // fim atualizar tabela


function DrawMacros(carboidrato,proteina,gordura) {





    google.charts.load('current', {'packages': ['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {

        var data = google.visualization.arrayToDataTable([
            ['Distribuição Macro', 'Macro'],
            ['Proteínas', proteina],
            ['Carboidratos', carboidrato],
            ['Gorduras', gordura]
        ]);

        var options = {
            title: 'Distribuição dos Macros',
            'width':430,
            'height':300
        };

        var chart = new google.visualization.PieChart(document.getElementById('chart_macros'));

        chart.draw(data, options);

    }
}








});